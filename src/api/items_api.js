

import axios from "axios";
import authHeader from "./data-service";

const GET_ITEMS='/api/menu_item/menu_items';
const EDIT_MENU_ITEM='/api/menu_item/edit_menu_item';
const CREATE_MENU_ITEM='/api/menu_item/add_menu_item';
const DELETE_MENU_ITEM='/api/menu_item/delete_menu_item';
const UPLOAD_FILE='/api/fileloader/uploadFile';
const ADD_EMPLOYEE='/api/add-employee/saveEmployee';
const ADD_CALCULATION='/api/add-calculation/saveCalculation';




export default {
    getMenuItems() {
        return axios.get(GET_ITEMS,{ headers: authHeader() })
    },

    editMenuItemApi(menuItem){
        return axios.post(EDIT_MENU_ITEM, menuItem,{ headers: authHeader() })
    },
    
    createMenuItemApi(menuItem){
        return axios.post(CREATE_MENU_ITEM, menuItem,{ headers: authHeader() })
    },
    
    deleteMenuItemApi(menuItemID){
        return axios.post(DELETE_MENU_ITEM +'?menuItemID='+menuItemID,{ headers: authHeader() })
    },
    
    uploadFileItemApi(file){
        let h = authHeader()
        h["Content-Type"]= "multipart/form-data"
        return axios.post(UPLOAD_FILE, file,{ headers: h })
    },
    
    addEmployee(employee){
        console.log(employee)
        return axios.post(ADD_EMPLOYEE, employee,{ headers: authHeader() })
    },
    
    addCalculation(calc){
        console.log(calc)
        return axios.post(ADD_CALCULATION, calc,{ headers: authHeader() })
    },
}
