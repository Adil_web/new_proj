import VueRouter from "vue-router";
import Login from '@/views/Login';
import Users from '@/components/admin/Users';
import Trash from '@/components/admin/Trash';

import PowerBI from "../components/powerbi/PowerBi";
import CustomComponent from "../components/powerbi/CustomComponent";


import Vue from 'vue';
import MainLayout from '@/components/shared/MainLayout';
import Calendar from '@/components/shared/Calendar';
import AdminLayout from "@/components/admin/AdminLayout";
import MenuItems from "../components/admin/MenuItems";
import Dictionary from "../components/admin/Dictionary";
import Shezhire from "../components/admin/Shezhire";
import Widget from "../components/admin/Widget";

Vue.use(VueRouter);

const router =  new VueRouter({
    mode: 'history',
    routes: [
        { path: '/main', name:'main', component: MainLayout, children:[
                {
                    path: 'home', name:'home', component:() => import('../components/engineering-department/briefcase/mainPage')
                },
                {
                    path: 'bi/:id', name: 'bi', component: PowerBI, props: true,
                },
                {
                    path: 'gmap', name: 'gmap', component: CustomComponent
                },
                {
                    path: 'calendar', name: 'calendar', component: Calendar
                },
                {
                    path: 'notifications', name:'notifications', component:() => import('../components/engineering-department/notifications/mainPage')
                },
                {
                    path: 'directory', name:'directory', component:() => import('../components/engineering-department/directory/mainPage')
                },
                {
                    path: 'all-users', name:'all-users', component:() => import('../components/engineering-department/all-users/mainPage')
                },
                {
                    path: 'logs', name:'logs', component:() => import('../components/engineering-department/logs/mainPage ')
                },
                {
                    path: 'reports', name:'reports', component:() => import('../components/engineering-department/reports/mainPage')
                },
            ]
        },
        { path: '/login', name:'login', component: Login},
        { path: '/news', name: 'news', component: () => import('../views/News')},
        { path: '/about', name: 'about', component: () => import('../views/About')},
        { path: '/our-team', name: 'team', component: () => import('../views/OurTeam')},
        { path: '/admin', name:'admin', component: AdminLayout, children:[
                {
                    path: 'users', name: 'users', component: Users
                },
                {
                    path: 'trash', name: 'trash', component: Trash
                },
                {
                    path: 'calendar', name: 'admin-calendar', component: Calendar
                },
                {
                    path: 'menuitems', name: 'menuitems', component: MenuItems
                },
                {
                    path: 'dictionary', name: 'dictionary', component: Dictionary
                },
                {
                    path: 'shezhire', name: 'shezhire', component: Shezhire
                },
                {
                    path: 'widget', name: 'widget', component: Widget
                },
                {
                    path: 'fileloader', name: 'fileloader', component: () => import('../components/admin/FileLoader')
                },
                {
                    path: 'add-employee', name: 'addEmployee', component: () => import('../components/admin/AddEmployee')
                },
                {
                    path: 'add-calc', name: 'addCalc', component: () => import('../components/admin/AddCalc')
                }
            ]
        },
    ]


});



router.beforeEach((to, from, next) => {
    const publicPages = ['/login', '/news', '/about', '/our-team'];

    const authRequired = !publicPages.includes(to.path);
    const loggedIn = sessionStorage.getItem('user-token');
    if (authRequired && !loggedIn ||to.name==null) {
        next('/login');
    } else {
        next();
    }
});

export default router;






